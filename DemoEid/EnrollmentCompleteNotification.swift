//
//  EnrollmentCompleteNotification.swift
//  Example
//
//  Created by Adrián Álvarez Fernández on 19/9/16.
//  Copyright © 2016 Electronic ID. All rights reserved.
//

import Foundation
import UIKit

class EnrollmentCompleteNotification {

    var label = UILabel()
    var shapeLayer = CAShapeLayer()

    init(labelText : String) {
    
        let win : CGRect = (UIApplication.shared.delegate?.window!!.bounds)!
        let rectangule = CGRect(x: (win.width) / 16, y: 10 * win.height / 12 , width: 7 * win.width / 8 , height: win.height / 12 )
        let path = UIBezierPath(rect: rectangule)
        self.shapeLayer.path = path.cgPath
        self.shapeLayer.fillColor = UIColor.colorCivitana().cgColor
        self.shapeLayer.strokeColor = UIColor.colorCivitanaBorder().cgColor
        self.shapeLayer.opacity=1
        self.label.frame = rectangule
        self.label.text = labelText
        
        self.label.adjustsFontSizeToFitWidth = true
        self.label.numberOfLines = 3
        self.label.textColor = UIColor.white
        self.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title2)
        
        
        self.label.textAlignment = .center
        UIApplication.shared.delegate?.window!!.rootViewController?.view.layer.addSublayer(self.shapeLayer)
        UIApplication.shared.delegate?.window!!.rootViewController?.view.addSubview(self.label)
    }
    
    func removeFromView() {
        
        self.label.removeFromSuperview()
        self.shapeLayer.removeFromSuperlayer()
        
    }
}



class WaitingNotification {
    
    var label = UILabel()
    
    init(labelText : String) {
        
        let win : CGRect = (UIApplication.shared.delegate?.window!!.bounds)!
        let rectangule = CGRect(x: (win.width) / 16, y: 5.5 * win.height / 12 , width: 7 * win.width / 8 , height: win.height / 12 )
 
        self.label.frame = rectangule
        self.label.text = labelText
        
        self.label.adjustsFontSizeToFitWidth = true
        self.label.numberOfLines = 3
        self.label.textColor = UIColor.white
        
        self.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title2)
        
        self.label.textAlignment = .center
        UIApplication.shared.delegate?.window!!.rootViewController?.view.addSubview(self.label)
    }
    
    func removeFromView() {
        
        self.label.removeFromSuperview()
        
    }
}
