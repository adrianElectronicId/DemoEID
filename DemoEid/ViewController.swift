import UIKit
import eIDVideoID

class ViewController: UIViewController, VideoIdViewControllerDelegate {
    
    enum states {
        case noScanning
        case scanningFace
        case scanningBackDNI
        case scanningFrontDNI
    }
    var videoId : VideoIdViewController?
    var videoConfig : VideoIdConfig!
    var token : String?
    
    var button : UIButton!
    var button2 : UIButton!
    var radioButton : CGFloat!
    var sizeOfTheWindow : CGRect!
    var icono : UIImageView!
    let duration : TimeInterval = 1
    var enrollmentSuccess : EnrollmentCompleteNotification?
    var waitingNotification : WaitingNotification?
    var actualState : states = states.noScanning
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.token = ""
        self.videoConfig = VideoIdConfig(apiEndpoint: "https://staging-etrust-live.electronicid.eu/v1", proxyEndpoint: "https://staging-etrust-live.electronicid.eu/v1")

        self.videoConfig.endpoints?.token = "Bearer 778adbe7-236e-4496-abbf-caef57ba976b"

        self.token = self.videoConfig.endpoints?.token!
        videoId = VideoIdViewController(config : self.videoConfig, delegate: self)
        self.addChildViewController(videoId!)
        videoId!.view.frame = self.view.bounds
        self.view.addSubview(videoId!.view)
        
        videoId!.didMove(toParentViewController: self)
        videoId!.turnOn()
        
        self.createButton()
        self.createButton2()
        
    }
    
    func createButton2() {
        self.button2 = UIButton(frame: CGRect(x: (self.view.bounds.width / 2) - (2 * radioButton), y: (self.view.bounds.height / 2 ) - (2 * radioButton), width: radioButton * 4, height: radioButton * 4))
        button2.setTitle("PULSA", for: UIControlState())
        button2.addTarget(self, action: #selector(buttonAction2), for: .touchUpInside)
    }
    
    func buttonAction2() {
        self.button2.removeFromSuperview()
        switch actualState {
        case states.noScanning:
            self.videoId?.scanFace()
            self.actualState = states.scanningFace
            break
        case states.scanningFace:
            self.videoId?.scanIdFront()
            self.actualState = states.scanningFrontDNI
            break
        case states.scanningFrontDNI:
            self.videoId?.scanIdBack()
            self.actualState = states.scanningBackDNI
            break
        case states.scanningBackDNI:
            self.videoId?.stopRecording()
            self.actualState = states.noScanning
            break
        }
    }
    
    func createWaterMark() {
        
        icono = UIImageView(image: UIImage(named: "eIDLogo.png"))
        icono.alpha = 0.5
        
        let newWidth = self.sizeOfTheWindow.maxX / 6 > 90 ? self.sizeOfTheWindow.maxX / 6 : 90
        
        icono = icono.reSizeImageView(Float(newWidth))
        icono.frame = CGRect(origin: CGPoint(x: self.sizeOfTheWindow.maxX - (1 * icono.bounds.width), y: self.sizeOfTheWindow.minY + (icono.bounds.height * 0.3)), size: CGSize(width: icono.bounds.width, height: icono.bounds.height))
        self.view.addSubview(icono)
        
    }
    
    func createButton() {
        
        self.sizeOfTheWindow = UIApplication.shared.delegate?.window!!.bounds
        self.radioButton = view.frame.height > view.frame.width ? (1.5 * (view.frame.width) / 12) : (1.5 * (view.frame.height) / 12)
        
        self.button = UIButton(type: .custom)
        
        button.frame = CGRect(x: (self.view.bounds.width / 2) - (2 * radioButton), y: (self.view.bounds.height / 2 ) - (2 * radioButton), width: radioButton * 4, height: radioButton * 4)
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.backgroundColor = UIColor.colorCivitanaGray()
        button.setTitle("Start", for: UIControlState())
        button.setTitleColor(UIColor.white, for: UIControlState())
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: radioButton)
    }
    
    func buttonAction(_ sender: UIButton!) {
        self.videoId!.startRecording()
        self.button.isUserInteractionEnabled = false
        if self.enrollmentSuccess != nil {
            enrollmentSuccess?.removeFromView()
        }
        UIView.animate(withDuration: 0.1 ,
                        animations: {
                            self.button.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        },
                        completion: { finish in
                            UIView.animate(withDuration: 0.1, animations: {
                            self.button.transform = CGAffineTransform.identity
                            })
        })
    }
    
    
    func videoIdTurnedOn() {
        self.view.addSubview(self.button)
        self.button.isUserInteractionEnabled = true
    }
    func videoIdConnected() {
        
        self.button.removeFromSuperview()
        
    }
    func videoIdRecordingStarted() {
        
        self.view.addSubview(self.button2)
        
    }
    func videoIdFaceDetectionFinished() {
        
        self.view.addSubview(self.button2)
        
    }
    func videoIdFrontDetectionFinished() {

        self.view.addSubview(self.button2)

    }
    func videoIdBackDetectionFinished() {

        self.videoId!.stopRecording()
        
    }
    func videoIdRecordingFinished(videoId : String) {

        self.postDataAndVerification(videoId)
        self.waitingNotification = WaitingNotification(labelText: "Waiting...")
        
    }
    
    func videoIdError(error: eIDVideoID.Error) {
        self.button2.removeFromSuperview()
        self.actualState = .noScanning
        self.showAlert("Error.", message: "Se ha producido un error.")
        self.videoId!.turnOff()
    }
    
    func videoIdGoBackground() {
        
    }
    
    func videoIdCameFromBackground() {
        self.actualState = .noScanning
        self.button.setTitle("Restart", for: UIControlState())
        self.view.addSubview(self.button)
        self.button.isUserInteractionEnabled = true
    }
    
    func showAlert(_ title : String, message : String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {(_) in
            
            alertView.dismiss(animated: true, completion: nil)
            self.videoId!.turnOn()
            self.actualState = .noScanning
            self.button.setTitle("Restart", for: UIControlState())
        }))
        
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    
    func postDataAndVerification(_ videoId : String) {
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(videoId ,forKey: "videoId")
        if !self.token!.isEmpty {
            para.setValue(self.token, forKey: "Authorization")
        }
        para.setValue("444e107c-5618-4096-a71e-47f249692a0c", forKey: "rauthorityId")

        let jsonData = try! JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions.init(rawValue: 0))
        let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
        
        self.postDataSynchronous("https://etrust-live.electronicid.eu/v1/verification_requests", bodyData: jsonString!) { (responseString, error) in
            if responseString != nil {
                var responseText = ""
                var success : Bool = false
                if (responseString?.contains("errorId"))! {
                    success = false
                    responseText = "Error"
                    
                } else {
                    
                    success = true
                    responseText = "Enrollment successful"
                    
                }
                
                DispatchQueue.main.async(execute: {
                    
                    if !success {
                        
                        self.enrollmentSuccess?.shapeLayer.fillColor = UIColor.red.withAlphaComponent(0.35).cgColor
                        self.enrollmentSuccess?.shapeLayer.strokeColor = UIColor.red.withAlphaComponent(0.75).cgColor
                        
                    } else {
                        
                        self.enrollmentSuccess?.shapeLayer.fillColor = UIColor.colorCivitana().cgColor
                        self.enrollmentSuccess?.shapeLayer.strokeColor = UIColor.colorCivitanaBorder().cgColor
                        
                        
                    }
                    self.enrollmentSuccess = EnrollmentCompleteNotification(labelText: responseText)
                    self.videoId!.turnOn()
                    self.waitingNotification?.removeFromView()
                    self.button.setTitle("Restart", for: UIControlState())
                    self.view.addSubview(self.button)
                    self.button.isUserInteractionEnabled = true
                    
                })
                
            }
            if error != nil {
                
            }
        }
    }
    
    fileprivate func postDataSynchronous(_ url: String, bodyData: String, completionHandler: @escaping (_ responseString: String?, _ error: NSError?) -> ())
    {
        
        let URL: Foundation.URL = Foundation.URL(string: url)!
        let request:NSMutableURLRequest = NSMutableURLRequest(url:URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if !self.token!.isEmpty {
            request.setValue(self.token, forHTTPHeaderField: "Authorization")
        }
        request.httpBody = bodyData.data(using: String.Encoding.utf8);
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        session.dataTask(with: request as URLRequest) {data, response, err in
            if data != nil {
                var output : String!
                output = String(data: data!, encoding: String.Encoding.utf8)
                completionHandler(output, err as NSError?)
            }
            if err != nil {
                
            }
            }.resume()
    }
    
    
}

extension UIColor {
    
    class func colorCivitana() -> UIColor {
        return UIColor(red: 86/255,green: 188/255, blue: 118/255, alpha: 0.35)
    }
    
    class func colorCivitanaBorder() -> UIColor {
        return UIColor(red: 86/255,green: 188/255, blue: 118/255, alpha: 0.75)
    }
    
    class func colorCivitanaTick() -> UIColor {
        return UIColor(red: 86/255,green: 188/255, blue: 118/255, alpha: 0.95)
    }
    
    class func colorCivitanaGray() -> UIColor {
        return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 0.55)
    }
    
    class func colorCivitanaYellow() -> UIColor {
        return UIColor(red: 240/255, green: 173/255, blue: 78/255, alpha: 0.70)
    }
    
}

extension UIImageView {
    
    func reSizeImageView(_ newWidth: Float) -> UIImageView{
        
        let oldWidth = self.bounds.width
        let ratio = newWidth / Float(oldWidth)
        
        let newHeight = Int(Float(self.bounds.height) * ratio)
        let aproxHeight = newHeight % 2 != 0 ? newHeight + 1 : newHeight
        
        let newCGSize = CGSize(width: CGFloat(newWidth), height: CGFloat(aproxHeight))
        
        self.frame = CGRect(origin: CGPoint.zero, size: newCGSize)
        
        return self
    }
}

